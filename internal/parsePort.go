package internal

import (
	"strconv"
)

// Address type alias
type Address string

// ParsePort gets a port number and gives an address to listen to
func ParsePort(port string) (Address, error) {
	_, err := strconv.ParseUint(port, 10, 16)
	if err != nil {
		return "", err
	}

	return Address(":" + port), nil
}
