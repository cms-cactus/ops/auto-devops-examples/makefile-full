package internal

import (
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
)

type fakeTimer struct{}

func (t fakeTimer) report() string {
	return "fake"
}

var expected = `<html>
<head>
  <title>rpm-demo-app</title>
</head>
<body>
  <p>I am running on hostname</p>
  <p>K8S_SECRET_DEMOSECRET at the time of deployment was "supersecret"</p>
  <p>I have been running for about fake</p>
</body>
</html>
`

func TestHandler(t *testing.T) {
	os.Setenv("HOSTNAME", "hostname")
	os.Setenv("DEMOSECRET", "supersecret")
	req, err := http.NewRequest("GET", "/", nil)
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(GetHandler(fakeTimer{}))
	handler.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v", status, http.StatusOK)
	}

	// Check the response body is what we expect.
	if rr.Body.String() != expected {
		t.Errorf("handler returned unexpected body: got %v want %v",
			rr.Body.String(), expected)
	}
}
