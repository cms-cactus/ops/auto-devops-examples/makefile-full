package internal

import (
	"fmt"
	"testing"
)

func TestParsePort(t *testing.T) {
	// 0 is a special port, instructs the kernel
	// to assign any free port
	// 65535 is the maximum
	for i := 0; i <= 65535; i++ {
		port := fmt.Sprintf("%v", i)
		address, err := ParsePort(port)
		if err != nil {
			t.Fatalf(`Parseport returned an error for input "%s": %e`, port, err)
		}
		if string(address) != ":"+port {
			t.Fatalf(`Parseport output invalid for input "%s": "%s"`, port, address)
		}
	}

	invalidPorts := []string{"-1", "", "lala", "65536", "inf", "999999", "-0"}
	for _, s := range invalidPorts {
		address, err := ParsePort(s)
		if err == nil {
			t.Fatalf(`Parseport did not return an error for input "%s", returned "%s"`, s, address)
		}
	}
}
