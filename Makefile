SHELL:=/bin/bash
.DEFAULT_GOAL := build

build:
	mkdir -p build
	go build -o build/serve ./cmd/serve.go

.PHONY: clean
clean:
	rm -rf build

test:
	mkdir -p build/coverage
	go test ./... -v -covermode=count -coverprofile=build/coverage/report.out 2>&1 | tee report.out
	go tool cover -html=build/coverage/report.out -o build/coverage/index.html

.PHONY: rpm
rpm: build
	# get last tag. if no previous tag exists, consider us at v0.0.1
	if git describe --tags --abbrev=0 > gittag; then echo; else echo "0.0.1" > gittag; fi;

	# get number of commits since last tag, or since start
	if git describe --tags --abbrev=0; then git rev-list --count $$(cat gittag)..HEAD > rev; else git rev-list --count HEAD > rev; fi;
	# increment
	echo $$(($$(cat rev) +1)) > rev

	echo $CI_SERVER_URL/$CI_PROJECT_PATH/

	# if [[ -z "$$(cat gittag)" ]]; then git rev-parse --short HEAD > gittag; fi
	# echo $$(($$(git rev-list --count $$(cat gittag)..HEAD) +1)) > rev
	cat gittag
	cat rev
	fpm \
		-s dir \
		-t rpm \
		-n demo-server \
		-v "$$(cat gittag)" \
		-m "<cactus@cern.ch>" \
		--iteration "$$(cat rev)" \
		--vendor CERN \
		--description "demo server" \
		--provides demo-server \
		--url $CI_REPOSITORY_URL \
		./build/=/opt/cactus/demoserver \
		./systemd/=/usr/lib/systemd/system
	mkdir -p build/rpm
	mv *.rpm build/rpm
	rm -rf gittag rev