FROM golang:alpine AS builder
RUN apk update && apk add --no-cache git
COPY . /project
WORKDIR /project
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 go build -ldflags="-w -s" -o /serve ./cmd/serve.go


FROM scratch
COPY --from=builder /serve /bin/serve
ENTRYPOINT ["/bin/serve"]
EXPOSE 80
