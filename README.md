[![pipeline status](https://gitlab.cern.ch/cms-cactus/ops/auto-devops-examples/makefile-full/badges/master/pipeline.svg)](https://gitlab.cern.ch/cms-cactus/ops/auto-devops-examples/makefile-full/-/commits/master)
[![coverage report](https://gitlab.cern.ch/cms-cactus/ops/auto-devops-examples/makefile-full/badges/master/coverage.svg?job=test)](https://gitlab.cern.ch/cms-cactus/ops/auto-devops-examples/makefile-full/-/commits/master)

# GitLab Cactus AutoDevOps Makefile Example

![pipeline example](doc/pipeline.png)

This project is an example implementation of a [GitLab CI configuration](.gitlab-ci.yml) that leverages [AutoDevOps](https://gitlab.cern.ch/cms-cactus/ops/auto-devops/) 
templates to implement commonly needed features.

To replicate this setup for your own project, see the [Setup section in this document](#setup)

## Quick links

- YUM repository: http://cactus.web.cern.ch/cactus/release/cms-cactus-ops-auto-devops-examples-makefile-full/master/centos7_x86_64/base/RPMS/
- code coverage report: http://cactus.web.cern.ch/cactus/release/cms-cactus-ops-auto-devops-examples-makefile-full/master/coverage/
- this documentation in HTML: https://docs.master.makefile-full.cmsl1test.juravenator.dev/
- This app on cactus-review: https://master.makefile-full.cmsl1test.juravenator.dev/

## Features

### Auto builds

- of a builder docker image, triggered when needed, with option to run manually
- of binaries, using make
- of documentation, using a markdown compiler template  
  [example of gitlab flavored markdown rendering](doc/dummy.md)
- of RPMs
- of a docker image containing compiled code

### Auto tests

- with code coverage reports, integrated with the GitLab UI
  ![coverage UI example](doc/merge-pipeline.png)
- integrated with GitLab test summaries
  ![unit test UI example](doc/test-visualization.png)

### Auto deploy

#### to EOS/CERNBox

- of RPMs to [a CERNBox/EOS backed yum repository](http://cactus.web.cern.ch/cactus/release/cms-cactus-ops-auto-devops-examples-makefile-full/master/centos7_x86_64/base/RPMS/)
  ![yum install example](doc/yum-repo.png)
- arbitrary file sync, such as static sites of [documentation](http://cactus.web.cern.ch/cactus/release/cms-cactus-ops-auto-devops-examples-makefile-full/master/docs/) and [code coverage reports](http://cactus.web.cern.ch/cactus/release/cms-cactus-ops-auto-devops-examples-makefile-full/0.0.3/coverage/)

#### to Kubernetes

This repository deploys to the [cactus review](https://gitlab.cern.ch/cms-cactus/ops/cactus-review) kubernetes cluster
- [documentation](https://docs.master.makefile-full.cmsl1test.juravenator.dev/) 
- [code coverage reports](https://coverage.master.makefile-full.cmsl1test.juravenator.dev/)
- the [actual code](https://master.makefile-full.cmsl1test.juravenator.dev/)
- Integrated with GitLab Environments & Review Apps
  ![coverage UI example](doc/merge-pipeline.png)
- Automatic SSL configuration using letsencrypt
  ![letsencrypt](doc/letsencrypt.png)
- Auto Monitoring
- Integration with GitLab secret variables
  ![CI variables](doc/ci-variables.png)
  ![deployed app with secret access](doc/app-deploy.png)

## Setup

### .gitlab-ci.yml
This project manually implements template jobs from the [AutoDevOps](https://gitlab.cern.ch/cms-cactus/ops/auto-devops/) repository.

Documentation and details on how to use each template can be found there.

### GitLab CI variables

To make certain features work, such as EOS sync and kubernetes deploys, it needs some extra variables that you will not find in `.gitlab-ci.yml`.

These missing variables are set in the GitLab UI.
The reason they are set in the UI, and not in `.gitlab-ci.yml`, is because these are sensitive variables.
![CI variables](doc/ci-variables.png)

- `AUTO_DEVOPS_CERNBOX_PASS` and `AUTO_DEVOPS_CERNBOX_USER` are used by EOS templates.
  Take note of the security recommendations in the [template docs](https://gitlab.cern.ch/cms-cactus/ops/auto-devops/-/blob/0.0.3/templates/upload-eos.md)
- `K8S_CONFIG` is used in Kubernetes templates. This is the content of a kubectl config file.  
  Note that this is a `file` type variable.
- `K8S_SECRET_DEMOSECRET` demonstrates the templates ability to auto-deploy secrets.


### Docker retention policies

Since we use the [docker-auto-tagging template](https://gitlab.cern.ch/cms-cactus/ops/auto-devops/-/blob/0.0.3/templates/docker-builder.md), we follow its documentation and configured our docker tag expiration policy to limit the number of docker tags in our repository.
![tag expiration config](doc/expiration-policy.png)